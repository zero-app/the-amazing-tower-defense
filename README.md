# The Amazing Tower Defense #

Tower defense game.

## Lab1 ##

Singleton pattern + 1 other pattern per team member.
Class diagram should consists of more than 10 classes.

Patterns that may be implemented:  
 * Singleton (one pattern per project).  
 * Factory.  
 * Abstract Factory.  
 * Strategy.  
 * Observer.  
 * Builder.  

## Lab2 ##

Adapter pattern + 1 other pattern per team member.
Class diagram should consist of more that 20 classes.

Patterns that may be implemented:  
 * Adapter (one pattern per project).  
 * Prorotype.  
 * Decorator.  
 * Command.  
 * Facade.  
 * Bridge.  
 
## Lab3 ##
 
Template method patter + 1 other pattern per team member.
Class diagram should consist of more than 30 classes.

Patterns that may be implemented:
 * Template method (one pattern per project).
 * Iterator.  
 * Composite.  
 * Flyweight.  
 * State.  
 * Proxy.  
 
## Lab4 ##

Chain of responsibility pattern and Null object pattern + 1 other patter per team member.
Class diagram should consist of more than 30 classes.
 
Patterns that may be implemented:
 * Chain of responsibility (one patter per project.  
 * Interpreter.  
 * Mediator.  
 * Memento.  
 * Visitor.  

## Members ##

Aivaras Dziaugys, Saulius Stankevicius, Sarunas Sarakojis.