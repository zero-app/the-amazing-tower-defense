package entity.trap;

public class InactiveTrapState implements ITrapState {
    @Override
    public void overlaps() {
        System.out.println("detached object overlaps with other object");
    }
}
