package entity.trap;

import entity.memento.Restorable;
import entity.memento.TrapMemento;
import entity.wallet.IMonetizeable;
import entity.wallet.ProtectedWallet;
import entity.wallet.Wallet;

public class Trap implements IMonetizeable, Restorable{
    private final double price = 5.0;
    private ITrapState state;

    public Trap() {
        state = new InactiveTrapState();
    }

    public void handleOverlapping(){
        state.overlaps();
    }

    public void setState(ITrapState state){
        this.state = state;
    }

    public void toggleState(){
        if (state.getClass() == PlacedTrapState.class){
            state = new InactiveTrapState();
        } else if (state.getClass() == InactiveTrapState.class){
            state = new PlacedTrapState();
        }
    }

    @Override
    public void visit(ProtectedWallet wallet) {
        System.out.println("Buying item for "+price);
        wallet.protectedCharge(this.price);
    }


    @Override
    public TrapMemento saveState() {
        return new TrapMemento(state);
    }

    @Override
    public void restoreState(TrapMemento restoreState) {
        restoreState.getState(this);
    }
}
