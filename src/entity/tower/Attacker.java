package entity.tower;

import entity.enemy.Enemy;

/**
 * This is a demo class, susceptible for deletion/modification.
 */
public class Attacker {

    private AttackTrigger attackTrigger;

    public Attacker(ITower attackingTower) {
        this.attackTrigger = new AttackAdapter(attackingTower);
    }

    public void attack(Enemy enemy) {
        attackTrigger.renderDamage(enemy);
    }
}
