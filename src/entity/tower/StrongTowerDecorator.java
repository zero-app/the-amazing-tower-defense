package entity.tower;

import javafx.scene.paint.Color;

public class StrongTowerDecorator extends TowerDecorator{

	public StrongTowerDecorator(ITower tower) {
		super(tower);
		changeColor();
	}
	private void changeColor() {
		tower.setColor(Color.RED);
	}
}
