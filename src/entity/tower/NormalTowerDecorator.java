package entity.tower;

import javafx.scene.paint.Color;

public class NormalTowerDecorator extends TowerDecorator {

	public NormalTowerDecorator(ITower tower) {
		super(tower);
		changeColor();
	}

	private void changeColor() {
		tower.setColor(Color.BLUE);
	}
}
