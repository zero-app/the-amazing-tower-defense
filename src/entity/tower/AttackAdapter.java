package entity.tower;

import entity.enemy.Enemy;

public class AttackAdapter implements AttackTrigger {

    private ITower attackingTower;

    public AttackAdapter(ITower attackingTower) {
        this.attackingTower = attackingTower;
    }

    @Override
    public void renderDamage(Enemy enemy) {
        attackingTower.attack(enemy);
    }
}
