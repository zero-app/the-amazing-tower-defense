package entity.tower;

import entity.dispatcher.ClusteredDamageDispatcher;
import entity.dispatcher.Dispatcher;
import entity.enemy.Enemy;
import entity.tower.upgrade.RangedTowerEnhancement;
import entity.tower.upgrade.TowerEnhancement;
import javafx.scene.paint.Color;

/**
 * RangedTower is a tower which attacks enemies on tiles in radius of 2.
 * It has slow attack speed but does more damage.
 */
public class RangedTower implements ITower {

    private double damage;
    private double speed;
    private Color color;
    private Dispatcher<Enemy, Double> damageDispatcher;
    private TowerEnhancement enhancement;

    public RangedTower() {
        this(new ClusteredDamageDispatcher());
    }

    public RangedTower(Dispatcher<Enemy, Double> damageDispatcher) {
        this(damageDispatcher, new RangedTowerEnhancement());
    }

    public RangedTower(Dispatcher<Enemy, Double> damageDispatcher, TowerEnhancement enhancement) {
        this.damageDispatcher = damageDispatcher;
        this.enhancement = enhancement;
        this.color = Color.GREEN;
        this.damage = 15d;
        this.speed = 20d;
    }

    public void upgrade() {
        enhancement.enhanceTower(this);

        speed = enhancement.getEnhancedSpeed();
        damage = enhancement.getEnhancedDamage();
    }

    public double getDamage() {
        return damage;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void attack(Enemy enemy) {
        damageDispatcher.dispatch(enemy, getDamage());
    }

    @Override
    public String toString() {
        return String.format("RangedTower[damage: %.1f, speed: %.1f]", damage, speed);
    }
}
