package entity.tower;

import javafx.scene.paint.Color;

public class WeakTowerDecorator extends TowerDecorator {

	public WeakTowerDecorator(ITower tower) {
		super(tower);
		this.changeColor();
	}

	private void changeColor() {
		tower.setColor(Color.GREEN);
	}
}
