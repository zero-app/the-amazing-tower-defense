package entity.tower.upgrade;

import entity.tower.ITower;

public class NullTowerEnhancement extends TowerEnhancement {

    @Override
    protected double enhanceDamage(ITower towerContext) {
        return towerContext.getDamage();
    }
}
