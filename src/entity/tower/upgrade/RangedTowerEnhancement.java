package entity.tower.upgrade;

import entity.tower.ITower;

public class RangedTowerEnhancement extends TowerEnhancement {

    @Override
    protected double enhanceSpeed(ITower towerContext) {
        return towerContext.getSpeed() * 1.5;
    }

    @Override
    protected double enhanceDamage(ITower towerContext) {
        return towerContext.getDamage() * 1.3;
    }
}
