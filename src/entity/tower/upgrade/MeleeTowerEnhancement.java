package entity.tower.upgrade;

import entity.tower.ITower;

public class MeleeTowerEnhancement extends TowerEnhancement {

    @Override
    protected double enhanceSpeed(ITower towerContext) {
        return towerContext.getSpeed() * 0.7;
    }

    @Override
    protected double enhanceDamage(ITower towerContext) {
        return towerContext.getDamage() * 1.8;
    }
}
