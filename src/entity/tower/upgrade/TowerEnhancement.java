package entity.tower.upgrade;

import entity.tower.ITower;

public abstract class TowerEnhancement {

    private double damage;
    private double speed;

    public void enhanceTower(ITower towerContext) {
        damage = enhanceDamage(towerContext);
        speed = enhanceSpeed(towerContext);
    }

    protected double enhanceSpeed(ITower towerContext) {
        return towerContext.getSpeed();
    }

    protected abstract double enhanceDamage(ITower towerContext);

    public double getEnhancedDamage() {
        return damage;
    }

    public double getEnhancedSpeed() {
        return speed;
    }
}
