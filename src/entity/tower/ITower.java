package entity.tower;

import entity.enemy.Enemy;
import javafx.scene.paint.Color;

/**
 * ITower object interface.
 * ITower attacks enemies within it's attack range.
 */
public interface ITower {

    void upgrade();

    double getDamage();

    double getSpeed();

    Color getColor();

    void setColor(Color color);

    void attack(Enemy enemy);
}
