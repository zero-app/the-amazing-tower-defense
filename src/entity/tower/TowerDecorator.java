package entity.tower;

import entity.enemy.Enemy;
import javafx.scene.paint.Color;

public abstract class TowerDecorator implements ITower {

    protected ITower tower;

    public TowerDecorator(ITower tower) {
        this.tower = tower;
    }

    @Override
    public void upgrade() {
        tower.upgrade();
    }

    @Override
    public double getDamage() {
        return tower.getDamage();
    }

    @Override
    public double getSpeed() {
        return tower.getSpeed();
    }

    @Override
    public Color getColor() {
        return tower.getColor();
    }

    @Override
    public void setColor(Color color) {
        tower.setColor(color);
    }

    @Override
    public void attack(Enemy enemy) {
        tower.attack(enemy);
    }
}
