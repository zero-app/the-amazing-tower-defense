package entity.tower;

import entity.enemy.Enemy;

public interface AttackTrigger {

    void renderDamage(Enemy enemy);
}
