package entity.tower;

import entity.dispatcher.Dispatcher;
import entity.dispatcher.IsolatedDamageDispatcher;
import entity.enemy.Enemy;
import entity.tower.upgrade.MeleeTowerEnhancement;
import entity.tower.upgrade.TowerEnhancement;
import javafx.scene.paint.Color;

/**
 * MeleeTower is a tower which attacks enemies on adjacent tiles.
 * MeleeTower has faster attack rate but does less damage.
 */
public class MeleeTower implements ITower {

    private double damage;
    private double speed;
    private Color color;
    private Dispatcher<Enemy, Double> damageDispatcher;
    private TowerEnhancement enhancement;

    public MeleeTower() {
        this(new IsolatedDamageDispatcher());
    }

    public MeleeTower(Dispatcher<Enemy, Double> damageDispatcher) {
        this(damageDispatcher, new MeleeTowerEnhancement());
    }

    public MeleeTower(Dispatcher<Enemy, Double> damageDispatcher, TowerEnhancement enhancement) {
        this.damageDispatcher = damageDispatcher;
        this.enhancement = enhancement;
        this.color = Color.GREEN;
        this.damage = 25d;
        this.speed = 80d;
    }

    public void upgrade() {
        enhancement.enhanceTower(this);

        speed = enhancement.getEnhancedSpeed();
        damage = enhancement.getEnhancedDamage();
    }

    public double getDamage() {
        return damage;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void attack(Enemy enemy) {
        damageDispatcher.dispatch(enemy, getDamage());
    }

    @Override
    public String toString() {
        return String.format("MeleeTower[damage: %.1f, speed: %.1f]", damage, speed);
    }
}
