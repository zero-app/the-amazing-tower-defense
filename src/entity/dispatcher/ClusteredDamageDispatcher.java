package entity.dispatcher;

import entity.enemy.Enemy;

import java.util.HashSet;
import java.util.Set;

public class ClusteredDamageDispatcher implements Dispatcher<Enemy, Double> {

    private Set<Enemy> targets;

    public ClusteredDamageDispatcher() {
        this(new HashSet<>());
    }

    public ClusteredDamageDispatcher(Set<Enemy> targets) {
        this.targets = targets;
    }

    @Override
    public boolean add(Enemy element) {
        return targets.add(element);
    }

    @Override
    public boolean remove(Enemy element) {
        return targets.remove(element);
    }

    @Override
    public void dispatch(Enemy target, Double sourceContext) {
        if (targets.contains(target)) {
            targets.parallelStream()
                    .forEach(enemy -> enemy.receiveDamage(sourceContext));
        }
    }
}
