package entity.dispatcher;

import entity.enemy.Enemy;

import java.util.HashSet;
import java.util.Set;

public class IsolatedDamageDispatcher implements Dispatcher<Enemy, Double> {

    private Set<Enemy> targets;

    public IsolatedDamageDispatcher() {
        this(new HashSet<>());
    }

    public IsolatedDamageDispatcher(Set<Enemy> targets) {
        this.targets = targets;
    }

    @Override
    public boolean add(Enemy element) {
        return targets.add(element);
    }

    @Override
    public boolean remove(Enemy element) {
        return targets.remove(element);
    }

    @Override
    public void dispatch(Enemy target, Double damage) {
        if (targets.contains(target)) {
            target.receiveDamage(damage);
        }
    }
}
