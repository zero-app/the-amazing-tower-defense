package entity.dispatcher;

public interface Dispatcher<T, S> {

    boolean add(T element);

    boolean remove(T element);

    void dispatch(T target, S sourceContext);
}
