package entity.timer;

import gameLogic.Observer;
import gameLogic.ObserverSubject;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class Timer extends Observer
{
    private ObserverSubject subject;

    private GraphicsContext graphicsContext;
    private long currentTime = 0;

    public Timer(GraphicsContext graphicsContext, ObserverSubject subject)
    {
        this.graphicsContext = graphicsContext;
        this.subject = subject;

        subject.attach(this);
    }

    public void update(long deltaTime)
    {
        currentTime += deltaTime;

        double width = graphicsContext.getCanvas().getWidth();
        double height = graphicsContext.getCanvas().getHeight();

        graphicsContext.setFont(Font.font(50));
        graphicsContext.setTextAlign(TextAlignment.CENTER);
        graphicsContext.fillText(Long.valueOf(currentTime / (long)1000000000.0).toString(), width / 2, height / 8);
    }
}
