package entity.spawn;

import entity.spawn.behaviour.EnemySpawnStrategy;

import java.util.List;

/**
 * Generator of enemies that delegates exact steps
 * to implementations.
 * Holds a collection of different strategies.
 */
public class EnemyGenerator {

    private List<EnemySpawnStrategy> enemySpawnStrategies;

    public EnemyGenerator(List<EnemySpawnStrategy> enemySpawnStrategies) {
        this.enemySpawnStrategies = enemySpawnStrategies;
    }

    public void executeSpawnAlgorithms() {
        enemySpawnStrategies.forEach(EnemySpawnStrategy::spawnEnemy);
    }

    public boolean addSpawnStrategy(EnemySpawnStrategy enemySpawnStrategy) {
        return enemySpawnStrategies.add(enemySpawnStrategy);
    }

    public boolean removeSpawnStrategy(Class<? extends EnemySpawnStrategy> spawnStrategyClass) {
        return enemySpawnStrategies
                .removeIf(strategy -> strategy.getClass().equals(spawnStrategyClass));
    }
}
