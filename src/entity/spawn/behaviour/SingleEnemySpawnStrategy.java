package entity.spawn.behaviour;

/**
 * An implementation of enemy spawn strategy that spawns an enemy
 * after a certain delay has passed.
 */
public class SingleEnemySpawnStrategy implements EnemySpawnStrategy {

    private long delayInMilliSeconds;

    public SingleEnemySpawnStrategy(long delayInMilliSeconds) {
        this.delayInMilliSeconds = delayInMilliSeconds;
    }

    @Override
    public void spawnEnemy() {
        // run on separate thread and wait for defined delay
        // then spawn an enemy (where !?)
        // will need to use an Enemy Builder that creates an enemy of certain type
    }
}
