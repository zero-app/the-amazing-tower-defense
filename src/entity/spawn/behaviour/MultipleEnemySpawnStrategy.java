package entity.spawn.behaviour;

/**
 * An implementation of enemy spawn strategy that spawns defined
 * number of enemies with a certain time period between spawn
 * events.
 */
public class MultipleEnemySpawnStrategy implements EnemySpawnStrategy {

    private long startDelay;
    private long periodBetweenExecutions;

    public MultipleEnemySpawnStrategy(long periodBetweenExecutions) {
        this(0L, periodBetweenExecutions);
    }

    public MultipleEnemySpawnStrategy(long startDelay, long periodBetweenExecutions) {
        this.startDelay = startDelay;
        this.periodBetweenExecutions = periodBetweenExecutions;
    }

    @Override
    public void spawnEnemy() {
        // use java.util.Timer for implementation
        // schedule execution at defined period after certain delay
        // spawn an enemy (where !?)
        // will need to use an Enemy Builder that creates an enemy of certain type
    }
}
