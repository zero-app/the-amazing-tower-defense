package entity.spawn.behaviour;

/**
 * Declares an algorithm of enemy generation. Particular
 * implementations define exact steps how the enemy should
 * be spawn.
 */
public interface EnemySpawnStrategy {

    void spawnEnemy();
}
