package entity.wallet;

public class ProtectedWallet extends Wallet {

    public ProtectedWallet() {
        super();
    }

    public void protectedCharge(double amount){
        double balance = this.getCredit();
        if (balance < amount){
            System.out.println("can't purchase item, insufficient balance");
        }else {
            this.chargeWallet(amount);
        }
    }

    public void buyItem(IMonetizeable item){
        item.visit(this);
    }
}
