package entity.wallet;

public interface IMonetizeable {
    void visit(ProtectedWallet wallet);
}
