package entity.wallet;

import java.time.LocalDateTime;

public class Wallet {
    private double credit;
    private LocalDateTime lastTransaction;

    public Wallet() {
        this.credit = 0.0;
        lastTransaction = null;
    }

    public void chargeWallet(double amount){
        credit -= amount;
        lastTransaction = LocalDateTime.now();
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public LocalDateTime getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(LocalDateTime lastTransaction) {
        this.lastTransaction = lastTransaction;
    }
}
