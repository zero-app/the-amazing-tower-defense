package entity;

import entity.wallet.ProtectedWallet;

/**
 * Singleton Player object.
 * This object is for player data: points, health, etc.
 */
public class Player {

    private static Player instance;

    private int points;
    private int health;
    private ProtectedWallet wallet;


    private Player() {
        wallet = new ProtectedWallet();
        points = 0;
        health = 100;
    }

    public ProtectedWallet getWallet() {
        return wallet;
    }

    public static Player getInstance() {
        return instance == null ? (instance = new Player()) : instance;
    }

    public int getPoints() {
        return points;
    }

    public int getHealth() {
        return health;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public void addHealth(int health) {
        this.health += health;
    }
}
