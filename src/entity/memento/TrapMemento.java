package entity.memento;

import entity.trap.ITrapState;
import entity.trap.Trap;

public class TrapMemento
{
   private ITrapState state;

    public TrapMemento(ITrapState state){
        this.state = state;
    }

    public void getState(Trap org) {
        org.setState(this.state);
    }
}
