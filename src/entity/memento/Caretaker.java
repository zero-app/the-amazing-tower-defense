package entity.memento;

import java.util.ArrayList;
import java.util.List;

public class Caretaker
{
    List<TrapMemento> statesList;

    public Caretaker()
    {
        statesList = new ArrayList<>();
    }

    public void add(TrapMemento state)
    {
        statesList.add(state);
    }

    public TrapMemento get(int index)
    {
        TrapMemento restoreState = statesList.get(index);
        statesList.remove(index);
        return restoreState;
    }

    public int size()
    {
        return statesList.size();
    }
}
