package entity.memento;

public interface Restorable
{
    TrapMemento saveState();
    void restoreState(TrapMemento restoreState);
}
