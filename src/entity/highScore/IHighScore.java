package entity.highScore;

import java.util.Date;

public interface IHighScore {
    int getScore();
    Date getDate();
    String getName();
}
