package entity.highScore;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HighScore implements IHighScore
{
    private final String file = "highScore.txt";
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    HighScore(int score, String name)
    {
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(file)));
            writer.write(name + "," + score + "," + dateFormat.format(new Date()));
            writer.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getScore()
    {
        int score = 0;

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
            String line = reader.readLine();

            score = Integer.parseInt(line.split(",")[1]);
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return score;
    }

    @Override
    public Date getDate()
    {
        Date date = new Date();

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
            String line = reader.readLine();

            //DateFormat dateFormat = new SimpleDateFormat();
            date = dateFormat.parse(line.split(",")[2]);
            reader.close();
        }
        catch (IOException | ParseException e)
        {
            e.printStackTrace();
        }

        return date;
    }

    @Override
    public String getName()
    {
        String name = "";

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
            String line = reader.readLine();

            name = line.split(",")[0];
            reader.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return name;
    }
}
