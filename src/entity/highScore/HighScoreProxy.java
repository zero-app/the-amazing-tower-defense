package entity.highScore;

import java.util.Date;

public class HighScoreProxy implements IHighScore
{
    private HighScore highScore;

    private int score;
    private String name;

    public HighScoreProxy(int score, String name)
    {
        this.score = score;
        this.name = name;
    }

    @Override
    public int getScore()
    {
       if(highScore == null)
       {
           highScore = new HighScore(score, name);
       }

       return highScore.getScore();
    }

    @Override
    public Date getDate()
    {
        if(highScore == null)
        {
            highScore = new HighScore(score, name);
        }

        return highScore.getDate();
    }

    @Override
    public String getName()
    {
        if(highScore == null)
        {
            highScore = new HighScore(score, name);
        }

        return highScore.getName();
    }
}
