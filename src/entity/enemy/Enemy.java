package entity.enemy;

/**
 *
 */
public interface Enemy {

    void receiveDamage(double damage);

    int getPoints();

    String getStatus();

    void setStatus(String status);

    Enemy clone();
}
