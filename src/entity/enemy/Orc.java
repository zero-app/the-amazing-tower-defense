package entity.enemy;

public class Orc implements Enemy {

    private double currentHealth;
    private int points = 15;
    private String status = "alive";

    public Orc() {
        this.currentHealth = 200d;
    }

    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Enemy clone() {
        return new Orc();
    }

    @Override
    public void receiveDamage(double damage) {
        currentHealth = Math.max(currentHealth - damage, 0d);
    }

    @Override
    public String toString() {
        return String.format("Orc[health: %s]", currentHealth);
    }
}
