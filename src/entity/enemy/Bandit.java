package entity.enemy;

public class Bandit implements Enemy {

    private int points = 10;
    private String status = "alive";
    private double currentHealth;

    public Bandit() {
        this.currentHealth = 100d;
    }

    @Override
    public Enemy clone() {
        return new Bandit();
    }

    @Override
    public int getPoints() {
        return points;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void receiveDamage(double damage) {
        currentHealth = Math.max(currentHealth - damage, 0d);
    }

    @Override
    public String toString() {
        return String.format("Bandit[health: %s]", currentHealth);
    }
}
