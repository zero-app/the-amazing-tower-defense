package gameLogic;

public abstract class Observer
{
    public abstract void update(long deltaTime);
}
