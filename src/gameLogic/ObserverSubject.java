package gameLogic;

import java.util.ArrayList;
import java.util.List;

public interface ObserverSubject
{
    List<Observer> updatables = new ArrayList<>();

    void attach(Observer updatable);

    void detach(Observer updatable);

    void notifyObservers(long deltaTime);

}
