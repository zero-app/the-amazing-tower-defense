package gameLogic;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import map.MapDrawer;

public class GameLoop extends AnimationTimer implements ObserverSubject
{
    private long deltaTime;
    private long lastUpdateTime = System.nanoTime();

    private GraphicsContext graphicsContext;
    private MapDrawer mapDrawer;

    public GameLoop(GraphicsContext graphicsContext, MapDrawer mapDrawer)
    {
        this.graphicsContext = graphicsContext;
        this.mapDrawer = mapDrawer;
    }

    public long getDeltaTime()
    {
        return deltaTime;
    }

    private void render()
    {
        graphicsContext.clearRect(0,0, graphicsContext.getCanvas().getWidth(), graphicsContext.getCanvas().getWidth());
        mapDrawer.drawSpriteMap();
    }

    @Override
    public void handle(long now)
    {
        deltaTime = now - lastUpdateTime;
        lastUpdateTime = now;

        this.render();
        this.notifyObservers(deltaTime);
    }

    @Override
    public void attach(Observer updatable)
    {
        updatables.add(updatable);
    }

    @Override
    public void detach(Observer updatable)
    {
        updatables.remove(updatable);
    }

    @Override
    public void notifyObservers(long deltaTime)
    {
        for(Observer updatable : updatables)
        {
            updatable.update(deltaTime);
        }
    }
}
