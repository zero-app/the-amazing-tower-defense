package service.handler;

import entity.Player;
import entity.enemy.Enemy;

public class ExplosionHandler extends AbstractDeathHandler {
    @Override
    public void handle(Enemy enemy) {
        Player player = Player.getInstance();
        player.addHealth(-enemy.getPoints());
        System.out.println(enemy.getPoints()+" health was lost on enemy explosion");
    }
}
