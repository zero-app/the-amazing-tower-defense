package service.handler;

import entity.Player;
import entity.enemy.Enemy;

public class KillHandler extends AbstractDeathHandler {

    @Override
    public void handle(Enemy enemy) {
        Player player = Player.getInstance();
        player.addPoints(enemy.getPoints());
        System.out.println(enemy.getPoints()+" points were added to player on enemy kill");
    }
}
