package service.handler;

import entity.enemy.Enemy;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDeathHandler {
    private List<AbstractDeathHandler> handlers;

    public AbstractDeathHandler(List<AbstractDeathHandler> handlers) {
        this.handlers = handlers;
    }

    public AbstractDeathHandler() {
        handlers = new ArrayList<>();
    }

    protected void addHandler(AbstractDeathHandler handler){
        this.handlers.add(handler);
    }

    protected AbstractDeathHandler getHandler(int pos){
        return handlers.get(pos);
    }

    public abstract void handle(Enemy enemy);
}
