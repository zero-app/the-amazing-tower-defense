package service.handler;

import entity.enemy.Enemy;

public class DeathHandler extends AbstractDeathHandler{

    public DeathHandler() {
        super();
        addHandler(new ExplosionHandler());
        addHandler(new KillHandler());
    }

    @Override
    public void handle(Enemy enemy) {
        if(enemy != null){
            if (enemy.getStatus().equals("killed")){
                getHandler(0).handle(enemy);
            } else if (enemy.getStatus().equals("exploded")){
                getHandler(1).handle(enemy);
            }
        }
    }
}
