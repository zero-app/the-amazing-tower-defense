package service.factory.image;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class IconPool {

    private static final int DEFAULT_SIZE = 1 << 4;

    private Map<String, Icon> iconsPool;

    public IconPool() {
        this(DEFAULT_SIZE);
    }

    public IconPool(int initialSize) {
        iconsPool = new HashMap<>(initialSize);
    }

    public Icon getIcon(String iconResource) {
        return iconsPool.computeIfAbsent(iconResource, this::loadImageIcon);
    }

    private ImageIcon loadImageIcon(String iconPath) {
        return new ImageIcon(iconPath) {
            @Override
            public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
                System.out.println(String.format("Icon painted at x=%d y=%d", x, y));
            }
        };
    }
}
