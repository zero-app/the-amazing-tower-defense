package service.factory.image;

public final class IconResources {

    public static final String MELEE_TOWER_ICON = "sprites/melee_tower.png";
    public static final String RANGED_TOWER_ICON = "sprites/ranged_tower.png";

    private IconResources() {
    }
}
