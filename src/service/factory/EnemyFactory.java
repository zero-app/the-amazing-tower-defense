package service.factory;

import entity.enemy.Bandit;
import entity.enemy.Enemy;
import entity.enemy.Orc;

import java.util.EnumMap;
import java.util.Map;

public class EnemyFactory {

    private static final Map<EnemyType, Enemy> ENEMY_PROTOTYPES;

    static {
        ENEMY_PROTOTYPES = new EnumMap<>(EnemyType.class);

        ENEMY_PROTOTYPES.put(EnemyType.BANDIT, new Bandit());
        ENEMY_PROTOTYPES.put(EnemyType.ORC, new Orc());
    }

    public enum EnemyType {
        BANDIT,
        ORC
    }

    private EnemyFactory() {
    }

    public static Enemy getEnemyPrototype(EnemyType enemyType) {
        return ENEMY_PROTOTYPES.get(enemyType).clone();
    }
}
