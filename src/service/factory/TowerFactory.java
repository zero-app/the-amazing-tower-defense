package service.factory;

import entity.tower.MeleeTower;
import entity.tower.RangedTower;
import entity.tower.ITower;

/**
 * Factory patter for tower creation.
 */
public class TowerFactory {

	public enum Type{
		MELEE_TOWER(1,"Melee"),RANGED_TOWER(2,"Ranged");
		private int number;
		private String name;

		Type(int number, String name) {
			this.number = number;
			this.name = name;
		}
	}

	/**
	 * Get tower by type specified.
	 * @param type - type of tower to return.
	 * @return new instance of specified tower.
	 */
	public ITower getTower(Type type){
		ITower result = null;
		switch (type){
			case MELEE_TOWER:
				result = new MeleeTower();
				break;
			case RANGED_TOWER:
				result = new RangedTower();
				break;
		}
		return result;
	}

}
