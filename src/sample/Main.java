package sample;

import entity.dispatcher.ClusteredDamageDispatcher;
import entity.dispatcher.Dispatcher;
import entity.dispatcher.IsolatedDamageDispatcher;
import entity.enemy.Bandit;
import entity.enemy.Enemy;
import entity.enemy.Orc;
import entity.highScore.HighScoreProxy;
import entity.memento.Caretaker;
import entity.memento.TrapMemento;
import entity.timer.Timer;
import entity.tower.ITower;
import entity.tower.MeleeTower;
import entity.tower.RangedTower;
import entity.tower.upgrade.NullTowerEnhancement;
import entity.trap.Trap;
import entity.wallet.ProtectedWallet;
import gameLogic.GameLoop;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import map.MapDrawer;
import service.factory.image.IconPool;
import service.factory.image.IconResources;
import service.handler.AbstractDeathHandler;
import service.handler.DeathHandler;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        String separator = "-----------------------------------------------";

        Canvas canvas = new Canvas(550, 550);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        root.getChildren().add(canvas);

        MapDrawer mapDrawer = new MapDrawer(gc, "./mapInfo.map");

        GameLoop gameLoop = new GameLoop(gc, mapDrawer);
        Timer timer = new Timer(gc, gameLoop);

        primaryStage.setTitle("Amazing ITower Defense");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();

        testStatePattern();
        System.out.println(separator);
        testVisitorPattern();
        System.out.println(separator);
        testFlyweightPattern();
        System.out.println(separator);
        testProxyPattern();
        System.out.println(separator);
        testMementoPattern();
        System.out.println(separator);
        testChainOfResponsibility();
        System.out.println(separator);
        testMediatorPattern();
        System.out.println(separator);
        testTemplateMethodPattern();
        System.out.println(separator);
        testNullObjectPattern();

        gameLoop.start();
    }


    public static void main(String[] args) {
        launch(args);
    }

    /**
     * State pattern implementation.
     */
    private void testStatePattern() {
        Trap trap = new Trap();

        System.out.println("State test:");
        trap.handleOverlapping();
        trap.toggleState();
        trap.handleOverlapping();
    }

    private void testVisitorPattern() {
        Trap trap = new Trap();
        ProtectedWallet wallet = new ProtectedWallet();

        System.out.println("Visitor test:");
        wallet.setCredit(100.0);
        System.out.println("Current wallet balance " + wallet.getCredit());
        wallet.buyItem(trap);
        System.out.println("Wallet balance after purchase " + wallet.getCredit());
    }

    private void testFlyweightPattern() {
        IconPool iconPool = new IconPool();
        long dt;

        System.out.println("Flyweight test:");
        dt = System.currentTimeMillis();
        Icon i1 = iconPool.getIcon(IconResources.MELEE_TOWER_ICON);
        System.out.println(String.format("First call to getIcon took: %.5f",
                (System.currentTimeMillis() - dt) / 1e3));

        i1.paintIcon(null, null, 0, 0);

        dt = System.currentTimeMillis();
        Icon i2 = iconPool.getIcon(IconResources.MELEE_TOWER_ICON);
        System.out.println(String.format("First call to getIcon took: %.5f",
                (System.currentTimeMillis() - dt) / 1e3));
        i2.paintIcon(null, null, 1, 1);
    }

    private void testProxyPattern() {
        HighScoreProxy highScore = new HighScoreProxy(55, "Saulius");
        System.out.println("Proxy test:");
        System.out.println(highScore.getName() + " " + highScore.getScore() + " " + highScore.getDate());
    }

    private void testMementoPattern() {
        Caretaker caretaker = new Caretaker();
        Trap trap = new Trap();

        TrapMemento state1 = trap.saveState();
        caretaker.add(state1);

        System.out.println("Memento test:");
        System.out.println("Original state:");
        trap.handleOverlapping();
        System.out.println("State change:");
        trap.toggleState();
        trap.handleOverlapping();

        System.out.println("State restore:");
        TrapMemento restoreState = caretaker.get(caretaker.size() - 1);
        trap.restoreState(restoreState);

        trap.handleOverlapping();
    }

    private void testChainOfResponsibility() {
        AbstractDeathHandler handler = new DeathHandler();
        Enemy enemy = new Bandit();

        System.out.println("Chain of responsibility test:");
        enemy.setStatus("killed");
        handler.handle(enemy);
        enemy.setStatus("exploded");
        handler.handle(enemy);
    }

    private void testMediatorPattern() {
        Set<Enemy> enemies = new HashSet<>();
        Dispatcher<Enemy, Double> d1 = new IsolatedDamageDispatcher(enemies);
        Dispatcher<Enemy, Double> d2 = new ClusteredDamageDispatcher(enemies);
        ITower t1 = new MeleeTower(d1);
        ITower t2 = new RangedTower(d2);
        Bandit e1 = new Bandit();
        Orc e2 = new Orc();

        enemies.add(e1);
        enemies.add(e2);

        System.out.println("Mediator test:");
        System.out.println("Enemy status before melee tower attach:");
        enemies.forEach(System.out::println);

        t1.attack(e1);

        System.out.println("Enemy status after melee tower attach:");
        enemies.forEach(System.out::println);

        t2.attack(e2);

        System.out.println("Enemy status after ranged tower attach:");
        enemies.forEach(System.out::println);
    }

    private void testTemplateMethodPattern() {
        List<ITower> towers = new ArrayList<>(4);

        towers.add(new MeleeTower());
        towers.add(new RangedTower());

        System.out.println("Template method test:");
        System.out.println("Towers before upgrade:");
        towers.forEach(System.out::println);

        towers.forEach(ITower::upgrade);

        System.out.println("Towers after upgrade:");
        towers.forEach(System.out::println);
    }

    private void testNullObjectPattern() {
        ITower tower = new MeleeTower(new IsolatedDamageDispatcher(), new NullTowerEnhancement());

        System.out.println("Null Object test:");
        System.out.println("Tower before upgrade:");
        System.out.println(tower);

        tower.upgrade();

        System.out.println("Tower after upgrade:");
        System.out.println(tower);
    }
}
