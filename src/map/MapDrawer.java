package map;

import javafx.scene.canvas.GraphicsContext;

/**
 * Facade class for drawing game map
 */
public class MapDrawer
{
    private IDrawableMap solidColourMap;
    private IDrawableMap spriteMap;

    private MapSpriteManager mapSpriteManager;

    public MapDrawer(GraphicsContext graphicsContext, String mapInfoFilePath)
    {
        MapInfo mapInfo = new MapInfo(mapInfoFilePath);
        mapSpriteManager = new MapSpriteManager();

        this.solidColourMap = new SolidColourMap(graphicsContext, mapInfo);
        this.spriteMap = new SpriteMap(graphicsContext, mapInfo, mapSpriteManager);
    }

    public void drawSolidColourMap()
    {
        solidColourMap.draw();
    }

    public void drawSpriteMap()
    {
        spriteMap.draw();
    }
}
