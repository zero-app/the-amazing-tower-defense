package map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

class MapInfo
{
    private ArrayList<ArrayList<Integer>> tileInfo = new ArrayList<>();

    MapInfo(String filePath)
    {
        readFile(filePath);
    }

    int getTile(int x, int y)
    {
        return tileInfo.get(y).get(x);
    }

    int getXSize()
    {
        return tileInfo.get(0).size();
    }

    int getYSize()
    {
        return tileInfo.size();
    }

    private void readFile(String filePath)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));

            String line;
            while((line = reader.readLine()) != null)
            {
                ArrayList<Integer> xTiles = new ArrayList<>();
                for (int i = 0; i < line.length(); i++)
                {
                    xTiles.add(Character.getNumericValue(line.charAt(i)));
                }
                tileInfo.add(xTiles);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();

        for (int y = 0; y < tileInfo.size(); y++)
        {
            for(int x = 0; x < tileInfo.get(y).size(); x++)
            {
                stringBuilder.append(tileInfo.get(y).get(x));
            }

            stringBuilder.append(System.lineSeparator());
        }

        return stringBuilder.toString();
    }
}
