package map;

import javafx.scene.image.Image;

class MapSpriteManager
{
    private final static String grassSpritePath = "file:./sprites/grass.jpeg";
    private final static String dirtSpritePath = "file:./sprites/dirt.jpeg";

    private Image grassSprite;
    private Image dirtSprite;

    MapSpriteManager()
    {
        this.grassSprite = new Image(grassSpritePath);
        this.dirtSprite = new Image(dirtSpritePath);
    }

    Image getGrassSprite()
    {
        return grassSprite;
    }

    Image getDirtSprite()
    {
        return dirtSprite;
    }
}
