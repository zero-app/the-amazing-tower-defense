package map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

class SolidColourMap implements IDrawableMap
{
    private GraphicsContext graphicsContext;
    private MapInfo mapInfo;

    SolidColourMap(GraphicsContext graphicsContext, MapInfo mapInfo)
    {
        this.graphicsContext = graphicsContext;
        this.mapInfo = mapInfo;
    }

    @Override
    public void draw()
    {
        double tileHeight = graphicsContext.getCanvas().getHeight() / (double)mapInfo.getYSize();
        double tileWidth = graphicsContext.getCanvas().getWidth() / (double)mapInfo.getXSize();

        for(int y = 0; y < mapInfo.getYSize(); y++)
        {
            for (int x = 0; x < mapInfo.getXSize(); x++)
            {
                int tileInfo = mapInfo.getTile(x, y);
                double currentX = (double)x * tileWidth;
                double currentY = (double)y * tileHeight;

                if(tileInfo == 0)
                {
                    graphicsContext.setFill(Color.LIGHTGREEN);
                    graphicsContext.fillRect(currentX, currentY, tileWidth, tileHeight);
                }
                else
                {
                    graphicsContext.setFill(Color.LIGHTGREY);
                    graphicsContext.fillRect(currentX, currentY, tileWidth, tileHeight);
                }
            }
        }
    }
}
