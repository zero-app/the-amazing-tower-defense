package map;

import javafx.scene.canvas.GraphicsContext;

class SpriteMap implements IDrawableMap
{
    private GraphicsContext graphicsContext;
    private MapInfo mapInfo;
    private MapSpriteManager spriteManager;

    SpriteMap(GraphicsContext graphicsContext, MapInfo mapInfo, MapSpriteManager spriteManager)
    {
        this.graphicsContext = graphicsContext;
        this.mapInfo = mapInfo;
        this.spriteManager = spriteManager;
    }

    @Override
    public void draw()
    {
        double tileHeight = graphicsContext.getCanvas().getHeight() / (double)mapInfo.getYSize();
        double tileWidth = graphicsContext.getCanvas().getWidth() / (double)mapInfo.getXSize();

        for(int y = 0; y < mapInfo.getYSize(); y++)
        {
            for (int x = 0; x < mapInfo.getXSize(); x++)
            {
                int tileInfo = mapInfo.getTile(x, y);
                double currentX = (double)x * tileWidth;
                double currentY = (double)y * tileHeight;

                if(tileInfo == 0)
                {
                    graphicsContext.drawImage(spriteManager.getGrassSprite(), currentX, currentY, tileWidth, tileHeight);
                }
                else
                {
                    graphicsContext.drawImage(spriteManager.getDirtSprite(), currentX, currentY, tileWidth, tileHeight);
                }
            }
        }
    }
}
